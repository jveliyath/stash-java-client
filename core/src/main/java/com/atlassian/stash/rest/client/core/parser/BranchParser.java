package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Branch;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Function;

class BranchParser implements Function<JsonElement, Branch> {
    @Override
    public Branch apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        return new Branch(
                jsonObject.get("id").getAsString(),
                jsonObject.get("displayId").getAsString(),
                jsonObject.get("latestChangeset").getAsString(),
                jsonObject.get("isDefault").getAsBoolean()
        );
    }
}

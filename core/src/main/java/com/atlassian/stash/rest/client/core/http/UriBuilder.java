package com.atlassian.stash.rest.client.core.http;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.function.BiConsumer;

/**
 * Simple UriBuilder, provided only to avoid external dependencies
 */
@NotThreadSafe
public class UriBuilder {
    private StringBuilder sb = new StringBuilder();
    private boolean queryParamExists = false;

    private UriBuilder() {
    }

    private UriBuilder setPath(@Nonnull final String pathFormat, @Nonnull final String... unencodedPathSegments) {
        sb.setLength(0);
        sb.append(String.format(pathFormat, (Object[]) Arrays
                .stream(unencodedPathSegments)
                .map(UriBuilder::encode)
                .toArray(String[]::new)));
        return this;
    }

    /**
     * factory method, generates new builder for given path.
     *
     * @param pathFormat            path format where path segments can be parametrised with <code>%s</code> like in
     *                              {@link String#format(String, Object...)}.
     * @param unencodedPathSegments unencoded path segments that will be encoded and inserted into <code>%s</code>
     *                              elements of the path format provided.
     * @return new {@link UriBuilder} instance.
     */
    @Nonnull
    public static UriBuilder forPath(@Nonnull final String pathFormat, @Nonnull final String... unencodedPathSegments) {
        return new UriBuilder().setPath(pathFormat, unencodedPathSegments);
    }

    /**
     * adds query parameter if given value is not null, encoding both name and value if necessary.
     *
     * @param name  name of parameter.
     * @param value value of the parameter.
     * @return this {@link UriBuilder} instance.
     */
    @Nonnull
    public UriBuilder addQueryParam(@Nonnull final String name, @Nullable final String value) {
        if (null == value) {
            return this;
        }
        addQueryParamSeparator();
        sb.append(encode(name))
                .append('=')
                .append(encode(value));
        return this;
    }

    /**
     * encode given object with provided function.
     *
     * @param t      object to be encoded.
     * @param mapper operation that would be given object to encode and this {@link UriBuilder} instance, to map this
     *               object into query parameters.
     * @param <T>    any type.
     * @return this {@link UriBuilder} instance.
     */
    @Nonnull
    public <T> UriBuilder encode(@Nullable final T t, @Nonnull final BiConsumer<UriBuilder, T> mapper) {
        if (null != t) {
            mapper.accept(this, t);
        }
        return this;
    }

    /**
     * converts current path and query params into {@link String}.
     *
     * @return encoded path and query params.
     */
    public String build() {
        return sb.toString();
    }

    private void addQueryParamSeparator() {
        if (!queryParamExists) {
            queryParamExists = true;
            sb.append('?');
        } else {
            sb.append('&');
        }
    }

    private static String encode(@Nonnull final String unencoded) {
        String result;
        try {
            result = URLEncoder.encode(unencoded, StandardCharsets.UTF_8.name()).replace("+", "%20");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("UTF-8 not supported", ex);
        }
        return result;
    }
}

package com.atlassian.stash.rest.client.api;

import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.atlassian.stash.rest.client.api.entity.Task;
import com.atlassian.stash.rest.client.api.entity.TaskAnchor;
import com.atlassian.stash.rest.client.api.entity.TaskState;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Optional;

public interface StashClient {
    /**
     * Get a list of all projects the user can see.
     *
     * @param start index of the first result to retrieve (for paging)
     * @param limit total number of results to retrieve after start (for Paging)
     * @return A page of Stash projects
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<Project> getAccessibleProjects(long start, long limit);


    /**
     * Search for repositories the user can see.  Repositories contain project information.
     *
     * @param projectKey optional. Filter by an individual project
     * @param query      Search term to filter repositories by
     * @param start      index of the first result to retrieve (for paging)
     * @param limit      total number of results to retrieve after start (for Paging)
     * @return A page of Stash repositories
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<Repository> getRepositories(@Nullable String projectKey, @Nullable String query,
                                     long start, long limit);


    /**
     * Retrieve a specific repository object
     *
     * @param projectKey     of the project the repo is in
     * @param repositorySlug slugified version of the repo name
     * @return A Stash repository or null if not found
     * @throws StashException
     * @throws StashRestException
     */
    @Nullable
    Repository getRepository(@Nonnull String projectKey, @Nonnull String repositorySlug);


    /**
     * Search for the branches for a specific repository
     *
     * @param projectKey     the repository belongs to
     * @param repositorySlug repository (this is the slugified version of the repository name, not the raw repository name)
     * @param query          search term for branch name
     * @param start          index of the first result to retrieve (for paging)
     * @param limit          total number of results to retrieve after start (for Paging)
     * @return A page of the repository branches
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<Branch> getRepositoryBranches(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                       @Nullable String query, long start, long limit);

    /**
     * Search for the tags for a specific repository
     *
     * @param projectKey     the repository belongs to
     * @param repositorySlug repository (this is the slugified version of the repository name, not the raw repository name)
     * @param query          search term for tag name
     * @param start          index of the first result to retrieve (for paging)
     * @param limit          total number of results to retrieve after start (for Paging)
     * @return A page of the repository tags
     * @throws StashException
     * @throws StashRestException
     */
    Page<Tag> getRepositoryTags(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                @Nullable final String query, final long start, final long limit);

    /**
     * Retrieves default branch for a specific repository
     *
     * @param projectKey     the repository belongs to
     * @param repositorySlug repository (this is the slugified version of the repository name, not the raw repository name)
     * @return repository branch
     */
    @Nullable
    Branch getRepositoryDefaultBranch(@Nonnull final String projectKey, @Nonnull final String repositorySlug);


    /**
     * Retrieves access keys (SSH public keys) for given repository
     *
     * @param projectKey     project key
     * @param repositorySlug repository key
     * @param start          index of the first result to retrieve (for paging)
     * @param limit          total number of results to retrieve after start (for Paging)
     * @return A page of repository access keys
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<RepositorySshKey> getRepositoryKeys(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                             long start, long limit);

    /**
     * Adds to the repository a new access key (SSH public key) with given permission
     *
     * @param projectKey     project key
     * @param repositorySlug repository key
     * @param publicKey      access key value
     * @param keyLabel       label stored with access key
     * @param keyPermission  type of permission given by public key
     * @return true on success
     * @throws StashException
     * @throws StashRestException
     * @throws StashUnauthorizedRestException
     */
    boolean addRepositoryKey(@Nonnull String projectKey, @Nonnull String repositorySlug, @Nonnull String publicKey,
                             @Nullable String keyLabel, @Nonnull Permission keyPermission);

    /**
     * Checks if provided access key (SSH public key) is already registered as a repository key on Stash server
     *
     * @param projectKey     project key
     * @param repositorySlug repository key
     * @param publicKey      Access key (SSH public key) value
     * @return true if key is already available on server, false if no key is provided or provided key is not registered as repository key on server
     * @throws StashException
     * @throws StashRestException
     */
    boolean isRepositoryKey(@Nonnull String projectKey, @Nonnull String repositorySlug, @Nonnull String publicKey);

    /**
     * Retrieves access keys (SSH public keys) for current user
     *
     * @param start index of the first result to retrieve (for paging)
     * @param limit total number of results to retrieve after start (for Paging)
     * @return A page of user access keys
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<UserSshKey> getCurrentUserKeys(long start, long limit);

    /**
     * Checks if provided access key (SSH public key) is already registered as a user key on Stash server
     *
     * @param publicKey Access key (SSH public key) value
     * @return true if key is already available on server, false if no key is provided or provided key is not registered as user key on server
     * @throws StashException
     * @throws StashRestException
     */
    boolean isUserKey(@Nonnull String publicKey);

    /**
     * Adds to current user a new access key (SSH public key)
     *
     * @param publicKey access key value
     * @param keyLabel  label stored with access key
     * @return true on success
     * @throws StashException
     * @throws StashRestException
     */
    boolean addUserKey(@Nonnull String publicKey, @Nullable String keyLabel);

    /**
     * Removes user key by key content and label
     *
     * @param publicKey public key to remove
     * @return true on success
     */
    boolean removeUserKey(@Nonnull String publicKey);

    /**
     * Removes user key by it's id
     *
     * @param keyId
     * @return true on success
     */
    boolean removeUserKey(long keyId);


    /**
     * Adds a project
     *
     * @param projectKey  project key to identify the project
     * @param name        display name of the project
     * @param type        project type, can be "NORMAL" or "PERSONAL"
     * @param description project description
     * @return true on success
     */
    boolean createProject(@Nonnull String projectKey, @Nonnull String name, @Nonnull String type, @Nonnull String description);

    /**
     * Adds a repository to project
     *
     * @param projectKey repository owning project key
     * @param name       name of the repository
     * @param forkable   true if repository should be forkable
     * @return true on success
     */
    boolean createRepository(@Nonnull String projectKey, @Nonnull String name, @Nonnull String scmId, boolean forkable);

    /**
     * Deletes a project of given projectKey
     *
     * @param projectKey key of the project to delete
     * @return true on success
     */
    boolean deleteProject(@Nonnull String projectKey);

    /**
     * Deletes a repository
     *
     * @param projectKey     owning project key
     * @param repositorySlug repository slug
     * @return true on success
     */
    boolean deleteRepository(@Nonnull String projectKey, @Nonnull String repositorySlug);

    /**
     * Retrieves stash application properties like version, buildNumber or buildDate
     *
     * @return A map of Stash application properties
     * @throws StashException
     * @throws StashRestException
     * @deprecated use {@link #getApplicationProperties()} instead.}
     */
    @Nonnull
    @Deprecated
    Map<String, String> getStashApplicationProperties();

    /**
     * Retrieves version information and other application properties.
     *
     * @return application properties
     */
    @Nonnull
    ApplicationProperties getApplicationProperties();

    /**
     * Retrieves user permission to given repository.
     * Current limitation is that we can only detect following states:
     * - REPO_ADMIN
     * - REPO_READ
     * - none
     *
     * @param projectKey     project key
     * @param repositorySlug repository key
     * @return Current user's permission for specified repository or null if user has no permissions
     */
    Optional<Permission> getCurrentUserRepositoryPermission(@Nonnull String projectKey, @Nonnull String repositorySlug);

    /**
     * Sets permission for specified user on specified repository.
     *
     * @param projectKey     project key to set permission on
     * @param repositorySlug repository slug to set permission on
     * @param userName       name of the user to set permission for
     * @param permission     permission to be set
     * @return true on success
     */
    boolean addRepositoryUserPermission(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                        @Nonnull String userName, @Nonnull Permission permission);

    /**
     * Creates a new pull request between two branches.
     *
     * @param title         pull request title
     * @param description   pull request description
     * @param fromRef       reference of the branch to be merged from
     * @param toRef         reference of the branch to be merged to
     * @param reviewers     collection of reviewers, each identified by user name
     * @param avatarRequest if provided remote Stash/Bitbucket will return avatars data as requested
     * @return status of newly created pull request
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    PullRequestStatus createPullRequest(@Nonnull String title, @Nullable String description,
                                        @Nonnull PullRequestRef fromRef, @Nonnull PullRequestRef toRef,
                                        @Nonnull Iterable<String> reviewers, @Nullable AvatarRequest avatarRequest);

    /**
     * Creates a new pull request between two branches.
     * No returned avatar data requested.
     *
     * @param title       pull request title
     * @param description pull request description
     * @param fromRef     reference of the branch to be merged from
     * @param toRef       reference of the branch to be merged to
     * @param reviewers   collection of reviewers, each identified by user name
     * @return status of newly created pull request
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    default PullRequestStatus createPullRequest(@Nonnull String title, @Nullable String description,
                                                @Nonnull PullRequestRef fromRef, @Nonnull PullRequestRef toRef,
                                                @Nonnull Iterable<String> reviewers) {
        return createPullRequest(title, description, fromRef, toRef, reviewers, null);
    }

    /**
     * Retrieve a page of pull requests to or from the specified repository.
     * No returned avatar data requested.
     *
     * @param projectKey     repository project key
     * @param repositorySlug repository slug
     * @param branchName     a fully-qualified branch ID to find pull requests to or from, such as
     *                       <code>refs/heads/master</code>
     * @param direction      the direction relative to the specified repository.
     * @param stateFilter    Supply ALL to return pull request in any state. If a state is supplied only pull requests
     *                       in the specified state will be returned.
     * @param order          the order to return pull requests in, either OLDEST (as in: "oldest first") or NEWEST.
     * @param start          index of the first result to retrieve (for paging)
     * @param limit          total number of results to retrieve after start (for Paging)
     * @param avatarRequest  if provided remote Stash/Bitbucket will return avatars data as requested
     * @return page of pull requests matching the criteria
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<PullRequestStatus> getPullRequestsForBranch(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                                     @Nonnull String branchName, @Nonnull PullRequestDirection direction,
                                                     @Nonnull PullRequestStateFilter stateFilter,
                                                     @Nonnull PullRequestsOrder order, long start, long limit,
                                                     @Nullable AvatarRequest avatarRequest);
    /**
     * Retrieve a page of pull requests to or from the specified repository.
     *
     * Note, as of BBS 4.10-4.12, the {@link PullRequestStatus#getMergeOutcome()} property might be missing if
     * BBS has not yet determined the mergeability of this pull request, as only cached value is returned if exists,
     * no actual check is being triggered by this call.  In order to find out actual mergeability state consider
     * calling {@link #canMergePullRequest(String, String, long)}.
     *
     * See also {@link StashVersions#REPORTS_PR_MERGE_STATUS}.
     *
     * @param projectKey     repository project key
     * @param repositorySlug repository slug
     * @param branchName     a fully-qualified branch ID to find pull requests to or from, such as
     *                       <code>refs/heads/master</code>
     * @param direction      the direction relative to the specified repository.
     * @param stateFilter    Supply ALL to return pull request in any state. If a state is supplied only pull requests
     *                       in the specified state will be returned.
     * @param order          the order to return pull requests in, either OLDEST (as in: "oldest first") or NEWEST.
     * @param start          index of the first result to retrieve (for paging)
     * @param limit          total number of results to retrieve after start (for Paging)
     * @return page of pull requests matching the criteria
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    default Page<PullRequestStatus> getPullRequestsForBranch(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                                             @Nonnull String branchName,
                                                             @Nonnull PullRequestDirection direction,
                                                             @Nonnull PullRequestStateFilter stateFilter,
                                                             @Nonnull PullRequestsOrder order, long start, long limit) {
        return getPullRequestsForBranch(projectKey, repositorySlug, branchName, direction, stateFilter, order, start,
                limit, null);
    }

    /**
     * Merge specified pull request.
     *
     * @param projectKey     repository project key
     * @param repositorySlug repository slug
     * @param pullRequestId  pull request id
     * @param version        pull request version, version of the data the decision to merge was based upon.
     * @param avatarRequest  if provided remote Stash/Bitbucket will return avatars data as requested
     * @return merged pull request data if merge was successful, exception otherwise.
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    PullRequestStatus mergePullRequest(@Nonnull String projectKey, @Nonnull String repositorySlug, long pullRequestId,
                                       long version, @Nullable AvatarRequest avatarRequest);

    /**
     * Merge specified pull request.
     * No returned avatar data requested.
     *
     * @param projectKey     repository project key
     * @param repositorySlug repository slug
     * @param pullRequestId  pull request id
     * @param version        pull request version, version of the data the decision to merge was based upon.
     * @return merged pull request data if merge was successful, exception otherwise.
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    default PullRequestStatus mergePullRequest(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                               long pullRequestId, long version) {
        return mergePullRequest(projectKey, repositorySlug, pullRequestId, version, null);
    }

    /**
     * Test whether a pull request can be merged.
     *
     * @param projectKey     repository project key
     * @param repositorySlug repository slug
     * @param pullRequestId  pull request id
     * @return expected outcome of merge operation if one was issued.
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    PullRequestMergeability canMergePullRequest(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                                long pullRequestId);


    /**
     * used to filter pull requests by state.
     */
    enum PullRequestStateFilter {
        OPEN,
        DECLINED,
        MERGED,
        ALL
    }

    /**
     * the direction relative to the specified repository.
     */
    enum PullRequestDirection {
        INCOMING,
        OUTGOING

    }

    /**
     * the order to return pull requests in, either OLDEST (as in: "oldest first") or NEWEST.
     */
    enum PullRequestsOrder {
        NEWEST,
        OLDEST
    }

    /**
     * Create a new repository forked from an existing repository.
     *
     * @param sourceProjectKey     key of the project where source repository belongs to
     * @param sourceRepositorySlug source repository to be forked from
     * @param targetProjectKey     key of the project where forked repository will be created.
     * @param targetRepositorySlug target repositor to be created
     * @return details of the forked repository
     */
    @Nonnull
    Repository forkRepository(@Nonnull String sourceProjectKey, @Nonnull String sourceRepositorySlug,
                              @Nonnull String targetProjectKey, @Nonnull String targetRepositorySlug);

    /**
     * adds general comment to a pull request
     *
     * @param projectKey     project key
     * @param repositorySlug repository slug
     * @param pullRequestId  pull request id
     * @param text           comment text
     * @return created comment
     */
    @Nonnull
    Comment addPullRequestGeneralComment(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                         long pullRequestId, @Nonnull String text);

    /**
     * Create new task
     *
     * @param anchor the anchor of the task, such as a pull request's comment
     * @param text   task text
     * @return created task
     */
    @Nonnull
    Task addTask(@Nonnull TaskAnchor anchor, @Nonnull String text);

    /**
     * Updates state and/or text of a task.
     * At least state or text need to be provided.
     *
     * @param taskId    task id
     * @param taskState optional state, if provided it will be updated to requested value.
     * @param text      optional text, if provided task text will be updated to requested value.
     * @return updated task.
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Task updateTask(long taskId, @Nullable TaskState taskState, @Nullable String text);

    /**
     * Updates state of a task.
     *
     * @param taskId    task id.
     * @param taskState desired state value.
     * @return updated task.
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    default Task updateTask(long taskId, @Nonnull TaskState taskState) {
        return updateTask(taskId, taskState, null);
    }

    /**
     * Updates state of a task.
     *
     * @param taskId task id.
     * @param text   desired text value.
     * @return updated task.
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    default Task updateTask(long taskId, @Nonnull String text) {
        return updateTask(taskId, null, text);
    }
}

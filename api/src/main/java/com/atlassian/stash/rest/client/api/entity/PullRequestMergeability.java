package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Describes whether a pull request can merge.
 */
public class PullRequestMergeability {
    @Nonnull
    private final Optional<String> outcome;
    private final boolean canMerge;
    private final boolean conflicted;

    public PullRequestMergeability(@Nonnull final Optional<String> outcome, final boolean canMerge, final boolean conflicted) {
        this.outcome = outcome;
        this.canMerge = canMerge;
        this.conflicted = conflicted;
    }

    /**
     * Note that due to lazy merge work success of the merge can not be guaranteed.
     *
     * @return expected outcome of merge operation if one was issued.
     * @since Bitbucket 4.10.0
     */
    @Nonnull
    public Optional<String> getOutcome() {
        return outcome;
    }

    /**
     * @return {@code false} if the pull request cannot be merged for any reason; otherwise {@code true}
     */
    public boolean isCanMerge() {
        return canMerge;
    }

    /**
     * @return returns {@code true} if the pull request has merge conflicts requiring resolution. A pull request with
     * merge conflicts cannot be merged.
     */
    public boolean isConflicted() {
        return conflicted;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("outcome", outcome)
                .add("canMerge", canMerge)
                .add("conflicted", conflicted)
                .toString();
    }
}
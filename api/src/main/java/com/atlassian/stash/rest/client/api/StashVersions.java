package com.atlassian.stash.rest.client.api;

import com.atlassian.stash.rest.client.api.StashClient.PullRequestDirection;
import com.atlassian.stash.rest.client.api.StashClient.PullRequestStateFilter;
import com.atlassian.stash.rest.client.api.StashClient.PullRequestsOrder;
import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;

import javax.annotation.Nonnull;

/**
 * history of particular features added to Stash/Bitbucket
 */
public enum StashVersions {
    /**
     * tasks support was added in Stash 3.3.0
     *
     * @since Stash 3.3.0
     */
    SUPPORTS_TASKS(3, 3, 0),
    /**
     * Stash product was renamed to Bitbucket at version 4.0.0
     *
     * @since Bitbucket 4.0.0
     */
    RENAMED_TO_BITBUCKET(4, 0, 0),
    /**
     * Pull request(s) data retrieved with
     * {@link StashClient#getPullRequestsForBranch(String, String, String, PullRequestDirection, PullRequestStateFilter,
     * PullRequestsOrder, long, long, AvatarRequest)}
     * may include mergeResult object representing expected outcome of merge operation if one was issued.
     * <p>
     * Note that due to lazy merge work success of the merge can not be guaranteed.
     * <p>
     * Also, merge result is retrieved only if found in cache (at least as of Bitbucket 4.12.0). One way to trigger
     * the check and cache result is to call {@link StashClient#canMergePullRequest(String, String, long)}.
     *
     * @since Bitbucket 4.10.0
     */
    REPORTS_PR_MERGE_STATUS(4, 10, 0);

    @Nonnull
    private final String buildNumber;

    StashVersions(final int major, final int minor, final int patch) {
        this.buildNumber = String.format("%d%03d%03d", major, minor, patch);
    }

    /**
     * tests given build number if it's older than defined by this enum
     *
     * @param aBuildNumber version number to examine, as retrieved with {@link StashClient#getApplicationProperties()}
     *                     in {@link ApplicationProperties#getBuildNumber()}
     * @return true iff version passed as an argument is older than this enum's version
     */
    public boolean givenVersionIsOlder(@Nonnull final String aBuildNumber) {
        return buildNumber.compareTo(aBuildNumber) > 0;
    }
}

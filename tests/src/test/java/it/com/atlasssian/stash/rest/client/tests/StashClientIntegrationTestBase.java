package it.com.atlasssian.stash.rest.client.tests;

import com.atlassian.stash.rest.client.api.AvatarRequest;
import com.atlassian.stash.rest.client.api.EntityMatchers.PullRequestStatusMatcherBuilder;
import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.api.StashRestException;
import com.atlassian.stash.rest.client.api.StashUnauthorizedRestException;
import com.atlassian.stash.rest.client.api.StashVersions;
import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.atlassian.stash.rest.client.api.entity.Task;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import junit.framework.AssertionFailedError;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.atlassian.stash.rest.client.api.EntityMatchers.applicationProperties;
import static com.atlassian.stash.rest.client.api.EntityMatchers.branch;
import static com.atlassian.stash.rest.client.api.EntityMatchers.comment;
import static com.atlassian.stash.rest.client.api.EntityMatchers.page;
import static com.atlassian.stash.rest.client.api.EntityMatchers.project;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestMergeability;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestParticipant;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestRef;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestStatus;
import static com.atlassian.stash.rest.client.api.EntityMatchers.repository;
import static com.atlassian.stash.rest.client.api.EntityMatchers.stashError;
import static com.atlassian.stash.rest.client.api.EntityMatchers.tag;
import static com.atlassian.stash.rest.client.api.EntityMatchers.task;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestDirection.INCOMING;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestDirection.OUTGOING;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestStateFilter.MERGED;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestStateFilter.OPEN;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestsOrder.NEWEST;
import static com.atlassian.stash.rest.client.api.StashVersions.RENAMED_TO_BITBUCKET;
import static com.atlassian.stash.rest.client.api.StashVersions.SUPPORTS_TASKS;
import static com.atlassian.stash.rest.client.api.entity.TaskState.RESOLVED;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_ADMIN_LOGIN;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_ADMIN_PASSWORD;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_USER_LOGIN;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.STASH_USER_PASSWORD;
import static it.com.atlasssian.stash.rest.client.tests.TestUtil.generateSshKey;
import static java.lang.Boolean.TRUE;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public abstract class StashClientIntegrationTestBase {

    // test set up
    protected StashClient service;

    private ForkedRepoPRFixture forkedRepoPRFixture;
    /**
     * dynamically retrieved version of the Stash/Bitbucket being tested
     */
    private String buildNumber;
    /**
     * either </code>/stash</code> or <code>/bitbucket</code>, depending on the product version being tested
     */
    private String appContext;

    @Before
    public void setUp() throws Exception {
        service = createStashClient(STASH_ADMIN_LOGIN, STASH_ADMIN_PASSWORD);
        forkedRepoPRFixture = new ForkedRepoPRFixture();
        buildNumber = service.getApplicationProperties().getBuildNumber();
        appContext = RENAMED_TO_BITBUCKET.givenVersionIsOlder(buildNumber) ? "/stash" : "/bitbucket";
    }

    @After
    public void tearDown() {
        service.deleteRepository(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug);
    }

    abstract protected StashClient createStashClient(String stashUsername, String stashPassword) throws Exception;

    @Test
    public void testGetAccessibleProjects() {
        // when
        Page<Project> projectPage = service.getAccessibleProjects(0, 10);

        // then
        assertThat(projectPage, page(Project.class).size(is(2)).build());
        List<Project> projects = Lists.newArrayList(projectPage.getValues());
        assertThat(projects.get(0), project()
                .key(is("BAM"))
                .name(is("Bamboo"))
                .description(is("Bamboo functional test data"))
                .selfUrl(is("http://localhost:7990" + appContext + "/projects/BAM"))
                .build()
        );
    }

    @Test
    public void testGetRepositories() {
        // when
        Page<Repository> repositoryPage = service.getRepositories(null, null, 0, 10);

        // then
        assertThat(repositoryPage, page(Repository.class).size(is(2)).build());

        List<Repository> repositories = Lists.newArrayList(repositoryPage.getValues());
        assertThat(repositories.get(0), repository()
                .slug(is("rep_1"))
                .name(is("rep_1"))
                .sshCloneUrl(is("ssh://git@localhost:7999/project_1/rep_1.git"))
                .httpCloneUrl(is("http://admin@localhost:7990" + appContext + "/scm/project_1/rep_1.git"))
                .selfUrl(is("http://localhost:7990" + appContext + "/projects/PROJECT_1/repos/rep_1/browse"))
                .project(project()
                        .selfUrl(is("http://localhost:7990" + appContext + "/projects/PROJECT_1"))
                        .build()
                )
                .build());
        assertThat(repositories.get(1), repository()
                .slug(is("sample-projects"))
                .name(is("Sample projects"))
                .sshCloneUrl(is("ssh://git@localhost:7999/bam/sample-projects.git"))
                .httpCloneUrl(is("http://admin@localhost:7990" + appContext + "/scm/bam/sample-projects.git"))
                .selfUrl(is("http://localhost:7990" + appContext + "/projects/BAM/repos/sample-projects/browse"))
                .project(project()
                        .selfUrl(is("http://localhost:7990" + appContext + "/projects/BAM"))
                        .build())
                .build());
    }

    @Test
    public void testGetRepositoriesWithSearch() {
        // when
        Page<Repository> repositoryPage = service.getRepositories(null, "sam", 0, 10);

        // then
        assertThat(repositoryPage, page(Repository.class).size(is(1)).build());

        List<Repository> repositories = Lists.newArrayList(repositoryPage.getValues());
        assertThat(repositories.get(0), repository()
                .slug(is("sample-projects"))
                .build());
    }

    @Test
    public void testGetRepository() {
        // when
        Repository repository = service.getRepository("project_1", "rep_1");

        // then
        assertThat(repository, repository()
                .slug(is("rep_1"))
                .name(is("rep_1"))
                .sshCloneUrl(is("ssh://git@localhost:7999/project_1/rep_1.git"))
                .httpCloneUrl(is("http://admin@localhost:7990" + appContext + "/scm/project_1/rep_1.git"))
                .selfUrl(is("http://localhost:7990" + appContext + "/projects/PROJECT_1/repos/rep_1/browse"))
                .project(project()
                        .selfUrl(is("http://localhost:7990" + appContext + "/projects/PROJECT_1"))
                        .build()
                )
                .build());
    }

    @Test
    public void testGetRepositoryIfNotExists() {
        // when
        Repository repository = service.getRepository("project_1", "NON_EXISTING_REPO");

        // then
        assertThat(repository, nullValue());
    }

    @Test
    public void testAddRepositoryKey() {
        // given
        TestUtil.SshKeyPair keyPair = generateSshKey(1024, "some label");
        boolean isKey = service.isRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey());
        assertThat("key should NOT exist before adding", isKey, is(false));

        // when
        boolean result = service.addRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey(), null, Permission.REPO_READ);

        // then
        assertThat("add call should be successful", result, is(true));
        isKey = service.isRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey());
        assertThat("key should exist after adding", isKey, is(true));
    }

    @Test
    public void testAddUserKey() {
        // given
        TestUtil.SshKeyPair keyPair = generateSshKey(1024, "some label");
        boolean isKey = service.isUserKey(keyPair.getPublicKey());
        assertThat("key should NOT exist before adding", isKey, is(false));

        // when
        boolean result = service.addUserKey(keyPair.getPublicKey(), null);

        // then
        assertThat("add call should be successful", result, is(true));
        isKey = service.isUserKey(keyPair.getPublicKey());
        assertThat("key should exist after adding", isKey, is(true));
    }

    @Test
    public void testFallbackAddRepositoryKeyToAddUserKey() {
        // given
        try {
            service = createStashClient(STASH_USER_LOGIN, STASH_USER_PASSWORD);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        TestUtil.SshKeyPair keyPair = generateSshKey(1024, "some label");

        // when
        try {
            service.addRepositoryKey("BAM", "sample-projects", keyPair.getPublicKey(), null, Permission.REPO_READ);
            fail("add ssh key to repository with user 'user' should fail");
        } catch (StashUnauthorizedRestException e) {
            // expected exception
        } catch (Throwable e) {
            fail("only StashUnauthorizedRestException should be thrown");
        }
        boolean addUserResult = service.addUserKey(keyPair.getPublicKey(), null);

        // then
        assertThat("add call should be successful", addUserResult, is(true));
        boolean isKey = service.isUserKey(keyPair.getPublicKey());
        assertThat("key should exist after adding", isKey, is(true));
    }

    @Test
    public void testGetRepositoryBranches() {
        // when
        Page<Branch> branchPage =
                service.getRepositoryBranches("BAM", "sample-projects", null, 0, 10);

        // then
        List<Branch> branches = Lists.newArrayList(branchPage.getValues());
        Map<String, Branch> branchMap = Maps.uniqueIndex(branches, STASH_BRANCH_ENTITY_TO_NAME::apply);
        assertThat(branchMap.keySet(), is(ImmutableSet.of("master", "feature-branch-x", "feature-branch-y",
                "branch_no_1", "branch_no_2")));
        assertThat(branchMap.get("master"), branch()
                .displayId(is("master"))
                .latestChangeset(is("9e8bd5114cd06c857a89651c329c1142a024f956"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetRepositoryTags() {
        // when
        Page<Tag> tag =
                service.getRepositoryTags("PROJECT_1", "rep_1", null, 0, 10);

        // then
        List<Tag> tags = Lists.newArrayList(tag.getValues());
        Map<String, Tag> tagMap = Maps.uniqueIndex(tags, STASH_TAG_ENTITY_TO_NAME::apply);
        assertThat(tagMap.keySet(), is(ImmutableSet.of("retagged_signed_tag", "signed_tag", "backdated_annotated_tag")));

        assertThat(tagMap.get("signed_tag"), tag()
                .displayId(is("signed_tag"))
                .id(is("refs/tags/signed_tag"))
                .latestChangeset(is("0a943a29376f2336b78312d99e65da17048951db"))
                .hash(is("12ebe2a58367347cd39f19f5a72f3cbec7b8f9a9"))
                .build()
        );
    }

    @Test
    public void testGetRepositoryDefaultBranch() {
        // when
        Branch branch = service.getRepositoryDefaultBranch("BAM", "sample-projects");

        // then
        assertThat(branch, branch()
                .displayId(is("master"))
                .latestChangeset(is("9e8bd5114cd06c857a89651c329c1142a024f956"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetRepositoryBranchesWithSearch() {
        // when
        Page<Branch> branchPage =
                service.getRepositoryBranches("BAM", "sample-projects", "feature-", 0, 10);

        // then
        List<Branch> branches = Lists.newArrayList(branchPage.getValues());
        Map<String, Branch> branchMap = Maps.uniqueIndex(branches, STASH_BRANCH_ENTITY_TO_NAME::apply);
        assertThat(branchMap.keySet(), is(ImmutableSet.of("feature-branch-x", "feature-branch-y")));
    }

    @Test
    public void testGetApplicationProperties() {
        // when
        final ApplicationProperties applicationProperties = service.getApplicationProperties();

        // then
        assertThat(applicationProperties, applicationProperties()
                .version(notNullValue())
                .buildDate(notNullValue())
                .buildNumber(notNullValue())
                .displayName(notNullValue())
                .build());
    }

    @Test
    public void testPullRequestCreateAndMerge() {
        final PullRequestStatus pullRequest = forkedRepoPRFixture.forkTestRepoAndCreateTestPR();

        assertThat("new PR can be found",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.sourceBranch, OUTGOING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(forkedRepoPRFixture.openPRMatcher))
                        .build());

        try {
            service.createPullRequest("WHATEVER", null, forkedRepoPRFixture.fromRef,
                    forkedRepoPRFixture.toRef, Collections.emptyList());
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_CONFLICT));
            assertThat(e.getErrors(), hasOnlyItem(stashError()
                    .message(is("Only one pull request may be open for a given source and target branch"))
                    .exceptionName(endsWith("DuplicatePullRequestException"))
                    .build()));
        }

        // attempt to merge with wrong PR version
        try {
            service.mergePullRequest(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                    pullRequest.getId(), pullRequest.getVersion() + 3);
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getErrors(), hasOnlyItem(stashError()
                    .message(is("You are attempting to modify a pull request based on out-of-date information."))
                    .exceptionName(endsWith("PullRequestOutOfDateException"))
                    .build()));
            assertThat(e.getStatusCode(), is(HTTP_CONFLICT));
        }

        final PullRequestStatus mergedPR = service.mergePullRequest(forkedRepoPRFixture.projectKey,
                forkedRepoPRFixture.forkedRepoSlug, pullRequest.getId(), pullRequest.getVersion());
        final Matcher<PullRequestStatus> mergedPRMatcher = forkedRepoPRFixture.prMatcherBuilder
                .id(is(pullRequest.getId()))
                .version(not(pullRequest.getVersion()))
                .build();
        assertThat("PR merged properly", mergedPR, mergedPRMatcher);

        assertThat("no open PR anymore initially",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.sourceRepo,
                        forkedRepoPRFixture.sourceBranch, OUTGOING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(0))
                        .build());

        assertThat("merged PR can still be found",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.sourceBranch, OUTGOING, MERGED, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(forkedRepoPRFixture.openPRMatcher))
                        .build());
    }

    @Test
    public void testCanMergePullRequest() {
        final PullRequestStatus pullRequest = forkedRepoPRFixture.forkTestRepoAndCreateTestPR();

        final PullRequestMergeability pullRequestMergeability =
                service.canMergePullRequest(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        pullRequest.getId());
        assertThat(pullRequestMergeability, pullRequestMergeability()
                .canMerge(is(Boolean.TRUE))
                .conflicted(is(Boolean.FALSE))
                .build());
    }

    @Test
    public void testCanMergePullRequest_mergeResultCachedAndReturnedInSubsequentGetPullRequestsForBranchCalls() {
        assumeStashVersionAtLeast(StashVersions.REPORTS_PR_MERGE_STATUS);

        final PullRequestStatus pullRequest = forkedRepoPRFixture.forkTestRepoAndCreateTestPR();

        final PullRequestMergeability pullRequestMergeability =
                service.canMergePullRequest(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        pullRequest.getId());
        assertThat(pullRequestMergeability, pullRequestMergeability()
                .canMerge(is(Boolean.TRUE))
                .conflicted(is(Boolean.FALSE))
                .outcome(is(Optional.of("CLEAN")))
                .build());

        assertThat("after call to canMergePullRequest merge result is cached and returned in following calls to " +
                        "getPullRequestsForBranch",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.sourceBranch, OUTGOING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(forkedRepoPRFixture.prMatcherBuilder
                                .mergeOutcome(is(Optional.of("CLEAN")))
                                .build()))
                        .build());
    }

    @Test
    public void testCommentCounts() {

        forkTestRepository();

        final PullRequestStatus pullRequest = service.createPullRequest("TEST_PR", "TEST DESC",
                forkedRepoPRFixture.fromRef, forkedRepoPRFixture.toRef, ImmutableList.of(forkedRepoPRFixture.testUser));
        assertThat("empty pull request created", pullRequest, pullRequestStatus()
                .commentCount(is(Optional.empty()))
                .build());

        assertThat("first comment added",
                service.addPullRequestGeneralComment(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        pullRequest.getId(), "1st comment"),
                comment()
                        .text(is("1st comment"))
                        .build());

        assertThat("comment count incremented to 1",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.targetBranch, INCOMING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(pullRequestStatus()
                                .commentCount(is(Optional.of(1L)))
                                .build()))
                        .build());

        assertThat("second comment added",
                service.addPullRequestGeneralComment(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        pullRequest.getId(), "2nd comment"),
                comment()
                        .text(is("2nd comment"))
                        .build());

        assertThat("comment count incremented to 2",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.targetBranch, INCOMING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(pullRequestStatus()
                                .commentCount(is(Optional.of(2L)))
                                .build()))
                        .build());
    }

    @Test
    public void testTasks() {
        assumeStashVersionAtLeast(SUPPORTS_TASKS);

        forkTestRepository();

        final PullRequestStatus pullRequest = service.createPullRequest("TEST_PR", "TEST DESC",
                forkedRepoPRFixture.fromRef, forkedRepoPRFixture.toRef, ImmutableList.of(forkedRepoPRFixture.testUser));
        assertThat("empty pull request created", pullRequest, pullRequestStatus()
                .outstandingTaskCount(is(Optional.empty()))
                .resolvedTaskCount(is(Optional.empty()))
                .build());

        final Comment comment = service.addPullRequestGeneralComment(forkedRepoPRFixture.projectKey,
                forkedRepoPRFixture.forkedRepoSlug, pullRequest.getId(), "1st comment");
        assertThat("comment added", comment, comment()
                .text(is("1st comment"))
                .build());

        assertThat("outstanding comment count is still zero after comment creation",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.targetBranch, INCOMING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(pullRequestStatus()
                                .outstandingTaskCount(is(Optional.of(0L)))
                                .resolvedTaskCount(is(Optional.of(0L)))
                                .build()))
                        .build());

        final Task task1 = service.addTask(comment, "1st task");
        assertThat("st 1task created", task1, task()
                .text(is("1st task"))
                .state(is("OPEN"))
                .commentAnchor(comment()
                        .id(is(comment.getId()))
                        .text(is("1st comment"))
                        .build())
                .build());

        assertThat("1 outstanding task reported",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.targetBranch, INCOMING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(pullRequestStatus()
                                .outstandingTaskCount(is(Optional.of(1L)))
                                .resolvedTaskCount(is(Optional.of(0L)))
                                .build()))
                        .build());

        final Task task2 = service.addTask(comment, "2nd task");
        assertThat("2nd task created", task2, task()
                .text(is("2nd task"))
                .state(is("OPEN"))
                .commentAnchor(comment()
                        .id(is(comment.getId()))
                        .text(is("1st comment"))
                        .build())
                .build());

        assertThat("2 outstanding tasks reported",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.targetBranch, INCOMING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(pullRequestStatus()
                                .outstandingTaskCount(is(Optional.of(2L)))
                                .resolvedTaskCount(is(Optional.of(0L)))
                                .build()))
                        .build());

        final Task task1Updated = service.updateTask(task1.getId(), RESOLVED, null);
        assertThat("1st task updated", task1Updated, task()
                .text(is("1st task"))
                .state(is("RESOLVED"))
                .commentAnchor(comment()
                        .id(is(comment.getId()))
                        .text(is("1st comment"))
                        .build())
                .id(is(task1.getId()))
                .build());

        assertThat("1 outstanding and 1 resolved tasks reported",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.targetBranch, INCOMING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(pullRequestStatus()
                                .outstandingTaskCount(is(Optional.of(1L)))
                                .resolvedTaskCount(is(Optional.of(1L)))
                                .build()))
                        .build());

        final Task task2Updated = service.updateTask(task2.getId(), RESOLVED, "new task text.");
        assertThat("2nd task updated", task2Updated, task()
                .text(is("new task text."))
                .state(is("RESOLVED"))
                .commentAnchor(comment()
                        .id(is(comment.getId()))
                        .text(is("1st comment"))
                        .build())
                .id(is(task2.getId()))
                .build());

        assertThat("No outstanding and 2 resolved tasks reported",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.targetBranch, INCOMING, OPEN, NEWEST, 0, 5),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(pullRequestStatus()
                                .outstandingTaskCount(is(Optional.of(0L)))
                                .resolvedTaskCount(is(Optional.of(2L)))
                                .build()))
                        .build());
    }

    @Test
    public void testAvatarsAbsolute() {
        final Matcher<PullRequestStatus> prMatcher = pullRequestStatus()
                .author(pullRequestParticipant()
                        .avatarUrl(startsWith("http"))
                        .build())
                .build();

        final AvatarRequest.DefaultAvatarRequest avatarRequest = AvatarRequest.builder().size(16).absolute(true).build();
        final PullRequestStatus pullRequestStatus = forkedRepoPRFixture.forkTestRepoAndCreateTestPR(
                avatarRequest);

        assertThat("avatarUrls returned as absolute", pullRequestStatus, prMatcher);

        assertThat("avatarUrls returned as absolute",
                service.getPullRequestsForBranch(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug,
                        forkedRepoPRFixture.sourceBranch, OUTGOING, OPEN, NEWEST, 0, 5, avatarRequest),
                page(PullRequestStatus.class)
                        .size(is(1))
                        .valuesIterable(hasOnlyItem(prMatcher))
                        .build());
    }
    
    private void assumeStashVersionAtLeast(final StashVersions expectedStashVersion) {
        Assume.assumeThat(buildNumber, stashVersionAtLeast(expectedStashVersion));
    }

    private Matcher<String> stashVersionAtLeast(final StashVersions expectedVersion) {
        return new BaseMatcher<String>() {
            String value;

            @Override
            public boolean matches(final Object o) {
                if (!(o instanceof String)) {
                    return false;
                }
                final String s = (String) o;
                if (expectedVersion.givenVersionIsOlder(s)) {
                    return false;
                }
                value = s;
                return true;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("expected Stash to be at least at version ")
                        .appendValue(buildNumber)
                        .appendText("but found version ")
                        .appendValue(value);
            }
        };
    }

    private Matcher<String> startsWith(final String prefix) {
        return new BaseMatcher<String>() {
            String error;

            @Override
            public boolean matches(Object o) {
                if (!(o instanceof String)) {
                    error = "Expected String, got " + ((null == o) ? "null" : o.getClass());
                    return false;
                }
                final String s = (String) o;
                if (!s.startsWith(prefix)) {
                    error = "Expected String starting with " + prefix;
                    return false;
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(error);
            }
        };
    }

    private Matcher<String> endsWith(String suffix) {
        return new BaseMatcher<String>() {
            String error;

            @Override
            public boolean matches(Object o) {
                if (!(o instanceof String)) {
                    error = "Expected String, got " + ((null == o) ? "null" : o.getClass());
                    return false;
                }
                final String s = (String) o;
                if (!s.endsWith(suffix)) {
                    error = "Expected String ending with " + suffix;
                    return false;
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(error);
            }
        };
    }

    private static <T> Matcher<Iterable<? extends T>> hasOnlyItem(Matcher<T> itemMatcher) {
        return new BaseMatcher<Iterable<? extends T>>() {
            String error;

            @Override
            public boolean matches(Object o) {
                if (!(o instanceof Iterable)) {
                    error = "expected Iterable, got " + ((null == o) ? "null" : o.getClass());
                    return false;
                }
                //noinspection unchecked
                final Iterator<T> iterator = ((Iterable) o).iterator();
                if (!iterator.hasNext()) {
                    error = "got empty collection";
                    return false;
                }
                final T first = iterator.next();
                if (!itemMatcher.matches(first)) {
                    error = "element mismatches";
                    return false;
                }
                if (iterator.hasNext()) {
                    error = "more than 1 element found, first unexpected: " + iterator.next();
                    return false;
                }
                return true;

            }

            @Override
            public void describeTo(Description description) {
                description.appendText(error).appendText(": ").appendDescriptionOf(itemMatcher);
            }
        };
    }

    private void forkTestRepository() {
        // set up test repository that we can play with
        final Repository repository = service.forkRepository(forkedRepoPRFixture.projectKey,
                forkedRepoPRFixture.sourceRepo, forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug);
        assertThat(repository, repository()
                .name(is(forkedRepoPRFixture.forkedRepoName))
                .slug(is(forkedRepoPRFixture.forkedRepoSlug))
                .project(project().
                        key(is(forkedRepoPRFixture.projectKey))
                        .build())
                .origin(repository()
                        .name(is(forkedRepoPRFixture.sourceRepo))
                        .slug(is(forkedRepoPRFixture.sourceRepo))
                        .project(project().
                                key(is(forkedRepoPRFixture.projectKey))
                                .build())
                        .build())
                .build());

        // give user access to test repo so they can participate in PR
        final boolean permGranted = service.addRepositoryUserPermission(forkedRepoPRFixture.projectKey,
                forkedRepoPRFixture.forkedRepoSlug, forkedRepoPRFixture.testUser, Permission.REPO_READ);
        assertThat(permGranted, is(TRUE));
    }

    public static final Function<Branch, String> STASH_BRANCH_ENTITY_TO_NAME = Branch::getDisplayId;

    public static final Function<Tag, String> STASH_TAG_ENTITY_TO_NAME = Tag::getDisplayId;

    class ForkedRepoPRFixture {
        /**
         * fork of rep1 repo used to test pull requests that may leave environment polluted in case of failure.
         * Deleted in {@see #tearDown} because of the above.
         */
        final String forkedRepoSlug = "rep1fork";
        final String forkedRepoName = "rep1fork";
        final String projectKey = "PROJECT_1";
        final String projectName = "Project 1";
        final String sourceRepo = "rep_1";
        final String testUser = "user";
        final String sourceBranch = "refs/heads/basic_branching";
        final String sourceBranchDisplayId = "basic_branching";
        final String targetBranch = "refs/heads/master";
        final String targetBranchDisplayId = "master";
        final PullRequestRef fromRef = new PullRequestRef(forkedRepoSlug, forkedRepoName, projectKey, projectName, sourceBranch);
        final PullRequestRef toRef = new PullRequestRef(forkedRepoSlug, forkedRepoName, projectKey, projectName, targetBranch);
        final PullRequestStatusMatcherBuilder prMatcherBuilder = pullRequestStatus()
                .title(is("TEST_PR"))
                .fromRef(pullRequestRef()
                        .displayId(is(sourceBranchDisplayId))
                        .id(is(sourceBranch))
                        .projectKey(is(projectKey))
                        .projectName(is(projectName))
                        .repositorySlug(is(forkedRepoSlug))
                        .repositoryName(is(forkedRepoName))
                        .build())
                .toRef(pullRequestRef()
                        .displayId(is(targetBranchDisplayId))
                        .id(is(targetBranch))
                        .projectKey(is(projectKey))
                        .projectName(is(projectName))
                        .repositorySlug(is(forkedRepoSlug))
                        .repositoryName(is(forkedRepoName))
                        .build())
                .author(pullRequestParticipant()
                        .name(is(STASH_ADMIN_LOGIN))
                        .role(is("AUTHOR"))
                        .build())
                .reviewers(hasOnlyItem(pullRequestParticipant()
                        .name(is(testUser))
                        .role(is("REVIEWER"))
                        .build()));
        final Matcher<PullRequestStatus> openPRMatcher = prMatcherBuilder.build();

        PullRequestStatus forkTestRepoAndCreateTestPR() {
            return forkTestRepoAndCreateTestPR(null);
        }

        PullRequestStatus forkTestRepoAndCreateTestPR(@Nullable final AvatarRequest avatarRequest) {
            forkTestRepository();

            assertThat("no PRs initially",
                    service.getPullRequestsForBranch(projectKey, sourceRepo, sourceBranch, OUTGOING, OPEN, NEWEST, 0, 5),
                    page(PullRequestStatus.class)
                            .size(is(0))
                            .build());

            // create a PR
            final PullRequestStatus pullRequest = service.createPullRequest("TEST_PR", "TEST DESC",
                    fromRef, toRef, ImmutableList.of(testUser), avatarRequest);
            assertThat("PR created correctly", pullRequest, openPRMatcher);
            return pullRequest;
        }
    }

}
